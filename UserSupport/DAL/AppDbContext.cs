﻿using System;
using Domain;
using Microsoft.EntityFrameworkCore;

namespace DAL
{
    public class AppDbContext :DbContext
    {
        public DbSet<Request> Requests { get; set; } = default!;

        public AppDbContext()
        {
            
        }
        

        
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
            optionsBuilder.UseSqlite("Data Source=database.db");
        }

        public AppDbContext(DbContextOptions options): base(options)
        {
        }
    }
}