﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public class CustomDateTimeValidation :ValidationAttribute 
    {
        public override bool IsValid(object value)  
        {  
            var dateTime = Convert.ToDateTime(value);  
            return dateTime >= DateTime.Now;  
        }  
    }
}