﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public class Request
    {
        public int Id { get; set; } = default!;
        [MinLength(1)] [MaxLength(512)] public string Description { get; set; } = default!;

        
        public DateTime TimePosted { get; set; } = default!;
        
        [Domain.CustomDateTimeValidation(ErrorMessage = "Request deadline cannot be in the past")]
        public DateTime Deadline { get; set; } = default!;

        public string? TextColor { get; set; }
    }
    
}