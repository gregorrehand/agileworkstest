﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DAL;
using Domain;
using Microsoft.EntityFrameworkCore;

namespace Repositories
{
    public class EFRequestRepository : IRequestRepository
    {
        private readonly AppDbContext _context;

        public EFRequestRepository(AppDbContext context)
        {
            _context = context;
        }

        public async Task<Request> GetByIdAsync(int id)
        {
            return await _context.Requests
                .FirstOrDefaultAsync(s => s.Id == id);
        }

        public async Task<List<Request>> ListAsync()
        {
           return await _context.Requests.ToListAsync();
        }

        public async Task AddAsync(Request request)
        {
            await _context.Requests.AddAsync(request);
            await _context.SaveChangesAsync();
        }

        public async Task DeleteAsync(int id)
        {
            _context.Requests.Remove(await GetByIdAsync(id));
            await _context.SaveChangesAsync();
        }
        
    }
}