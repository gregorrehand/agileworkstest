﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Domain;

namespace Repositories
{
    public interface IRequestRepository
    {
        Task<Request> GetByIdAsync(int id);
        Task<List<Request>> ListAsync();
        Task AddAsync(Request request);
        Task DeleteAsync(int id);
    }
}