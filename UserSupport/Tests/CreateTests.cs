﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Domain;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Repositories;
using WebApp.Controllers;
using Xunit;

namespace Tests
{
    public class CreateTests
    {

        [Fact]
        public async Task RequestsCanBeSuccessfullyCreated()
        {
            /*
             See test ebaõnnestub, kuna var result = await controller.Create(request); mockitud repoga ei loo 
             päriselt uut Requesti. Ma teen repot mockides midagi valesti ja ma ei suutnud leida oma viga leida.
             Rakenduses funktsionaalsus töötab.
             */
            
            // Arrange
            var mockRepo = new Mock<IRequestRepository>();
            mockRepo.Setup(repo => repo.ListAsync());
            var controller = new RequestsController(mockRepo.Object);

            // Act
            const int id = 3;
            var request = new Request() {Id = id, Description = "Test one", Deadline = new DateTime(2023, 4, 3), TimePosted = new DateTime(9999, 1, 1)};
            var result = await controller.Create(request);
            
            // Assert
            var redirectToActionResult = Assert.IsType<RedirectToActionResult>(result);
            Assert.Null(redirectToActionResult.ControllerName);
            Assert.Equal("Index", redirectToActionResult.ActionName);
            mockRepo.Verify();
            var model = mockRepo.Object.ListAsync();
            Assert.NotNull(model);
            Assert.NotNull(model.Result);
            request.TimePosted = model.Result.Where(r => r.Id.Equals(id)).ToList()[0].TimePosted;
            Assert.Contains(request, model.Result);
        }
        
        
        [Fact]
        public void TestIfUserCannotEnterPastDateAsDeadline()
        {
            var request = new Request() 
            {
                Id = 1,
                Description = "Test",
                Deadline = DateTime.Now.AddHours(-1)
            };
            Assert.Contains(ValidateModel(request), v => v.MemberNames.Contains("Deadline") && 
                     v.ErrorMessage.Contains("Request deadline cannot be in the past"));
        }
        
        [Fact]
        public void TestIfUserCannotEnterEmptyDescription()
        {
            var request = new Request() 
            {
                Id = 1,
                Description = "",
                Deadline = DateTime.Now.AddHours(1)
            };
            Assert.Contains(ValidateModel(request), v => v.MemberNames.Contains("Description") && 
                                                         v.ErrorMessage.Contains("The field Description must be a string or array type with a minimum length of '1'."));
        }

        private IList<ValidationResult> ValidateModel(object model)
        {
            var validationResults = new List<ValidationResult>();
            var ctx = new ValidationContext(model, null, null);
            Validator.TryValidateObject(model, ctx, validationResults, true);
            return validationResults;
        }

    }
}