using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Domain;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Repositories;
using WebApp.Controllers;
using Xunit;

namespace Tests
{
    public class IndexTests
    {
        [Fact]
        public async Task RequestsAreOrderedByDeadlineInIndex()
        {
            // Arrange
            var mockRepo = new Mock<IRequestRepository>();
            mockRepo.Setup(repo => repo.ListAsync()).ReturnsAsync(GetTestRequests1());
            var controller = new RequestsController(mockRepo.Object);

            // Act
            var result = await controller.Index();

            // Assert
            var viewResult = Assert.IsType<ViewResult>(result);
            var model = (List<Request>)viewResult.Model;
            Assert.NotNull(model);
            Assert.Equal("Test two", model[0].Description);
            Assert.Equal("Test three", model[1].Description);
            Assert.Equal("Test one", model[2].Description);
        }
        private static List<Request> GetTestRequests1()
        {
            var requests = new List<Request>
            {
                new Request() {Id = 1, Description = "Test one", Deadline = new DateTime(2023, 4, 3)},
                new Request() {Id = 2, Description = "Test two", Deadline = new DateTime(2021, 4, 3)},
                new Request() {Id = 3, Description = "Test three", Deadline = new DateTime(2022, 4, 3)},
            };
            return requests;
        }
        
        [Fact]
        public async void TestIfRequestsAreRed_WhenTimeToDeadlineIsLessThanOneHour()
        {
            // Arrange
            var mockRepo = new Mock<IRequestRepository>();
            mockRepo.Setup(repo => repo.ListAsync()).ReturnsAsync(GetTestRequests2());
            var controller = new RequestsController(mockRepo.Object);

            
            // Act
            var result = await controller.Index();

            // Assert
            var viewResult = Assert.IsType<ViewResult>(result);
            var model = (List<Request>)viewResult.Model;
            Assert.Equal("red", model[0].TextColor);
            Assert.Equal("red", model[1].TextColor);
            Assert.Null(model[2].TextColor);
        }


        private static List<Request> GetTestRequests2()
        {
            var requests = new List<Request>
            {
                new Request() {Id = 1, Description = "Should be red", Deadline = DateTime.Now.AddHours(-30)},
                new Request() {Id = 2, Description = "Should be red", Deadline = DateTime.Now.AddMinutes(30)},
                new Request() {Id = 3, Description = "Should not be red", Deadline = DateTime.Now.AddHours(30)},



            };
            return requests;
        }
    }
}