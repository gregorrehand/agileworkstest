﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Domain;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Repositories;
using WebApp.Controllers;
using Xunit;

namespace Tests
{
    public class SolveTests
    {
        [Fact]
        public async Task RequestsCanBeSuccessfullySolved()
        {
            /*
            See test ebaõnnestub, kuna var result = await controller.Solve(id); mockitud repoga ei kustuta 
            päriselt valitud Requesti (ka deletedRequest muutja on null). Ma teen repot mockides midagi valesti
             ja ma ei suutnud leida oma viga leida. Rakenduses funktsionaalsus töötab.
            */
            
            // Arrange
            var mockRepo = new Mock<IRequestRepository>();
            mockRepo.Setup(repo => repo.ListAsync()).ReturnsAsync(GetTestRequests());
            var controller = new RequestsController(mockRepo.Object);

            // Act
            const int id = 1;
            var deletedRequest = mockRepo.Object.GetByIdAsync(id).Result;
            var result = await controller.Solve(id);
            await mockRepo.Object.DeleteAsync(id);

            // Assert
            var redirectToActionResult = Assert.IsType<RedirectToActionResult>(result);
            Assert.Null(redirectToActionResult.ControllerName);
            Assert.Equal("Index", redirectToActionResult.ActionName);
            mockRepo.Verify();
            var model = mockRepo.Object.ListAsync();
            Assert.NotNull(model);
            Assert.NotNull(deletedRequest);
            Assert.DoesNotContain(deletedRequest, model.Result);
            Assert.Equal(2,model.Result.Count);
        }
        
        
        
        private static List<Request> GetTestRequests()
        {
            var requests = new List<Request>
            {
                new Request() {Id = 1, Description = "Test one", Deadline = new DateTime(2023, 4, 3)},
                new Request() {Id = 2, Description = "Test two", Deadline = new DateTime(2021, 4, 3)},
                new Request() {Id = 3, Description = "Test three", Deadline = new DateTime(2022, 4, 3)},
            };
            return requests;
        }
    }
}