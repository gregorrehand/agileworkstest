using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DAL;
using Domain;
using Repositories;

namespace WebApp.Controllers
{
    public class RequestsController : Controller
    {
        private readonly IRequestRepository _requestRepository;

        public RequestsController(IRequestRepository requestRepository)
        {
            _requestRepository = requestRepository;
        }

        // GET: Requests
        public async Task<IActionResult> Index()
        {
            var requests = await _requestRepository.ListAsync();
            var sortedRequests = requests.OrderBy(a => a.Deadline).ToList();
            foreach (var request in sortedRequests.Where(request => request.Deadline.Subtract(DateTime.Now).Hours <= 1))
            {
                request.TextColor = "red";
            }
            return View(sortedRequests);
        }
        

        // GET: Requests/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Requests/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Description,Deadline")] Request request)
        {
            request.TimePosted = DateTime.Now;
            if (!ModelState.IsValid) return View(request);
            await _requestRepository.AddAsync(request);
            return RedirectToAction(nameof(Index));
        }
        

        // POST: Requests/Delete/5
        [HttpPost, ActionName("Solve")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Solve(int id)
        {
            await _requestRepository.DeleteAsync(id);
            return RedirectToAction(nameof(Index));
        }
        
    }
}
